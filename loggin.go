package logging

import (
	"os"
	"strconv"

	"github.com/mattn/go-colorable"
	"github.com/sirupsen/logrus"
)

var (
	Log *logrus.Logger
)

func init() {
	Log = logrus.New()
	debugLevel := os.Getenv("LOG_LEVEL")

	if debugLevel != "" {

		level, err := strconv.Atoi(debugLevel)
		if err != nil {
			logrus.Fatal("Error occured", err)
		}
		Log.SetLevel(logrus.Level(level))

	} else {
		Log.SetLevel(logrus.WarnLevel)
	}

	Log.SetFormatter(&logrus.TextFormatter{ForceColors: true,
		TimestampFormat: "2006-01-02 15:04:05",
		FullTimestamp:   true,
	})

	Log.SetOutput(colorable.NewColorableStdout())
}
