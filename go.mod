module gitlab.com/ganeshdipdumbare_golang/logging

go 1.12

require (
	github.com/mattn/go-colorable v0.1.2 // indirect
	github.com/sirupsen/logrus v1.4.2 // indirect
)
